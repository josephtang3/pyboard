#led test 
#with on intensity() on LED4
import pyb
from pyb import Timer
from pyb import Switch
from pyb import Accel


# LED loop test
def LED_loop_test():
    for i in range(1, 5):
        #pyb.LED(i).on()
        pyb.LED(i).toggle()
        pyb.delay(100)
        #pyb.LED(i).off()
        pyb.LED(i).toggle()
        pyb.delay(100)

LED_loop_test()
LED_loop_test()

led = pyb.LED(4)
#start the LED intensity value with 0
intensity = 254
try:
    while True:
        intensity = (intensity + 1) % 255
        #print(intensity)
        led.intensity(intensity)
        #show the effect quickly with a shorter delay
        pyb.delay(30)
finally:
    #turn off LED
    #when interrupt branch here, eg ctrl c
    #print("finally")
    pyb.LED(4).off()
